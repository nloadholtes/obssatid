#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Nick Loadholtes on 08/28/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

import unittest
from Observatory import Observatory

class ObservatoryTests(unittest.TestCase):
    sample = "Code  Long.    cos       sin     Name\n\
    000   0.0000  0.62411  +0.77873  Greenwich\n\
    001   0.1542  0.62992  +0.77411  Crowborough\n\
    002   0.62    0.622    +0.781    Rayleigh two name\n\
    003   3.90    0.725    +0.687    Montpellier"
    
    def setUp(self):
        pass
    
    def testParseData(self):
        """docstring for testParseData"""
        o = Observatory()
        data = o.parseData(self.sample)
        self.assertEquals(5, len(data))
        print data
        
    def testConvertToPolar(self):
        lat = 51.477811
        from math import sin, cos, pi, atan
        c = cos(lat *(pi/180))
        s = sin(lat *(pi/180))
        clat = atan(s/c) * (180/pi)
        # print "Lat:",lat
        # print "c,s=>",c,s, clat
        # print "Should be: 0.62411 +0.77873 51.477811"
        self.assertEquals(lat, clat)

if __name__ == '__main__':
    unittest.main()