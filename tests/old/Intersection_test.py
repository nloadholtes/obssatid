#!/usr/bin/env python

import unittest
from Intersection import Intersection
import pysgp4
from Observatory import Observatory

class IntersectionTest(unittest.TestCase):
    def setup(self):
        pass
    
    def getMocks(self):
        sat = pysgp4.satPos()
        sat.x = 10
        sat.y = 10
        sat.z = 10
        obs = Observatory()
        obs.x = 1
        obs.y = 1
        obs.z = 1
        return obs, sat
    
    def testIntersection(self):
        inter = Intersection()
        obs, sat = self.getMocks()
        self.assertTrue(inter.isVisible(obs, sat, 20))
        
    def testNoIntersection(self):
        inter = Intersection()
        obs, sat = self.getMocks()
        self.assertFalse(inter.isVisible(obs, sat, 10))
        
    def testSatelliteRaDec(self):
        inter = Intersection()
        obs, sat = self.getMocks()
        (ra,dec, dist) = inter.satelliteRaDec(obs, sat)
        self.assertAlmostEqual(0.78539816339744828, ra)
        self.assertEqual(0, dec)
        self.assertAlmostEqual(15.588457268119896, dist)
