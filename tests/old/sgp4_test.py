#!/usr/bin/env python
# encoding: utf-8
"""
sgp4_test.py

This class is meant to exercise the SGP4 code that is wrapped
by the pysgp4.py module.

Created by Nick Loadholtes on 05/29/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

import unittest
import pysgp4

# bool sgp4init
#      (
#        gravconsttype whichconst,  char opsmode,  const int satn,     const double epoch,
#        const double xbstar,  const double xecco, const double xargpo,
#        const double xinclo,  const double xmo,   const double xno,
#        const double xnodeo,  elsetrec& satrec
#      );
 
#void twoline2rv
#     (
#      char      longstr1[130], char longstr2[130],
#      char      typerun,  char typeinput, char opsmode,
#      gravconsttype       whichconst,
#      double& startmfe, double& stopmfe, double& deltamin,
#      elsetrec& satrec
#     )

#['SGP4Version', '__builtins__', '__doc__', '__file__', '__name__', '__package__', '_newclass', '_object', 
#'_pysgp4', '_swig_getattr', '_swig_property', '_swig_repr', '_swig_setattr', '_swig_setattr_nondynamic', 
#'elsetrec', 'elsetrec_swigregister', 'getgravconst', 'gstime', 'new', 'new_instancemethod', 'pi', 'sgp4', 'sgp4init',
# 'twoline2rv', 'wgs72', 'wgs72old', 'wgs84']

@unittest.skip()
class sgp4_test(unittest.TestCase):
    def setUp(self):
        self.p = pysgp4

    def testInit(self):
        p = pysgp4
        self.assertTrue(None != p)
    
    def testwgs84(self):
        """docstring for testwgs84"""
        self.assertEqual(2, self.p.wgs84)
        
    def testwgs72old(self):
        """docstring for testwgs72old"""
        self.assertEqual(0, self.p.wgs72old)
        
    def testwgs72(self):
        """docstring for testwgs72"""
        self.assertEqual(1, self.p.wgs72)
        
    def testpi(self):
        """docstring for testpi"""
        self.assertEqual(3.1415926535897931, self.p.pi)
        
    def testVersion(self):
        self.assertEqual("SGP4 Version 2008-11-03", self.p.SGP4Version)
        
    def testParseSatData(self):
        line1 = ""
        line2 = ""
        typerun = ""
        typeinput = ""
        opsmode = ""
        gravconsttype = self.p.wgs84
        startmfe = 0
        stopmfe = 0
        deltamin = 0
        satrec = None
        satrec = self.p.tleParser(line1, line2,
                        typerun, typeinput,
                        opsmode, gravconsttype)     
        self.assertNotEqual(None, satrec)
        
    def testSGP4Wrapper(self):
        p = self.p
        whichconst = p.wgs72
        satrec = self.p.tleParser(
            "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753", 
            "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667     0.00      4320.0        360.00",
            "c", "e", "i", whichconst)
        print satrec
        satpos = p.sgp4wrapper(whichconst,
                            satrec,
                            0)
        self.assertNotEqual(None, satpos)
        print satpos.x, satpos.y, satpos.z
        self.assertAlmostEquals(7022.46529266, satpos.x)
        self.assertAlmostEquals(-1400.08296755,satpos.y)
        self.assertAlmostEquals(0.03995155, satpos.z)


if __name__ == '__main__':
    unittest.main()