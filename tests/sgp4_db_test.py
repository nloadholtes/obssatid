#!/usr/bin/env python
# encoding: utf-8
"""
sgp4_db_test.py

Created by Nick Loadholtes on 07/08/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

import unittest
import sqlite3

class SGP4DbTest(unittest.TestCase):

    def testWhat(self):
        pass

tc = SGP4DbTest("testWhat")

@unittest.skip
def testLoad():
    import pysgp4
    conn = sqlite3.connect('tests/obs.db')
    cur = conn.cursor()
    cur.execute("select id,t,x,y,z from sattimes ")
    t = TLEReader('tests/SGP4-VER.TLE')
    t.createMap()
    m = t.satmap
    p = pysgp4
    for pos in cur:
        # print "Pos=",pos
        xyz = pos[2:]
        t = pos[1]
        satid = pos[0]
        # print "SatId", satid , "Time:",t
        # print "XYZ", xyz
        # if satid == 23599 and t > 400: #This sat does not work for some reason
        #     continue
        satrec = p.tleParser(
            m[satid][0],m[satid][1],
            "c", "e", "i", p.wgs72)
        satpos = p.sgp4wrapper(p.wgs72,
                               satrec,
                               t)
        yield checkPos, xyz, satpos
    conn.close()


def testLoad_sgp4():
    from sgp4 import io
    from sgp4 import earth_gravity
    from sgp4.propagation import sgp4
    conn = sqlite3.connect('tests/obs.db')
    cur = conn.cursor()
    cur.execute("select id,t,x,y,z from sattimes ")
    t = TLEReader('tests/SGP4-VER.TLE')
    t.createMap()
    m = t.satmap
    for pos in cur:
        xyz = pos[2:]
        t = pos[1]
        satid = pos[0]
        if satid == 23599 and t > 400:  # This sat does not work for some reason
            continue
        satrec = io.twoline2rv(
            m[satid][0], m[satid][1], earth_gravity.wgs72)
        satpos, vel = sgp4(satrec, pos[1])
        yield checkPos, xyz, satpos
    conn.close()


def checkPos(xyz, satpos):
    tc.assertAlmostEqual(xyz[0], satpos[0])
    tc.assertAlmostEqual(xyz[1], satpos[1])
    tc.assertAlmostEqual(xyz[2], satpos[2])


def testTLEReader():
        t = TLEReader('tests/SGP4-VER.TLE')
        assert 33 == len(t.satellites)


class SatResultData():
    '''Class to hold the Satellite test data'''
    def __init__(self):
        pass


class TLEReader():
    '''This is a class to read in the TLE buffer, and divy it up real quickly\
    so we can do basic work with it. '''
    def __init__(self, filename):
        f = open(filename)
        buffer = f.read()
        if not buffer:
            return None
        lines = buffer.splitlines()
        x = 0
        output = []
        self.satellites = []
        tle = None
        while x < len(lines):
            line = lines[x]
            firstchar = line[0]
            if "1" == firstchar:
                output.append(line)
                tle = True
            if "2" == firstchar:
                if tle:
                    output.append(line)
                    tle = None
                    self.satellites.append(output)
                    output = []
            x += 1
        f.close()

    def __iter__(self):
        return self.satellites.__iter__()

    def createMap(self):
        m = {}
        for x in self.satellites:
            id = int(x[0][2:7])
            m[id] = x
        self.satmap = m


if __name__ == '__main__':
    unittest.main()
