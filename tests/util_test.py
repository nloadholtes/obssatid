#!/usr/bin/env python
# encoding: utf-8
"""
util_test.py

Created by Nick Loadholtes on 08/28/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

import unittest
import util
from math import pi as PI

class utilTest(unittest.TestCase):
    def setUp(self):
        pass
        
    def testd2r(self):
        """docstring for testd2r"""
        self.assertEqual(0, util.d2r(0))
        self.assertEqual(PI, util.d2r(180))
        
    def testr2d(self):
        """docstring for r2d"""
        self.assertEqual(90, util.r2d(PI/2))
        
    def testd2h(self):
        """docstring for d2h"""
        self.assertEqual("11 30\' 0.0\"", util.d2h(11.5))
        
    def testh2d(self):
        """docstring for h2d. This function isn't really implemented, so this test is here just to make sure
        that nothing funky happens (or changes unexpectedly)"""
        self.assertEqual(None, util.h2d(12))

    def testLatLongToXYZ(self):
        """Tests converting lat and long into xyz"""
        xyz = util.latLongToXYZ(0,0)
        self.assertEqual((util.EARTH_RADIUS_KM,0,0), xyz)
        xyz = util.latLongToXYZ(1,2)
        self.assertNotEqual((0,0,0), xyz)
        xyz = util.latLongToXYZ(44.01, -69.9)
        self.assertAlmostEqual(1576.4608373, xyz[0])
        self.assertAlmostEqual(-4307.88170487, xyz[1])
        self.assertAlmostEqual(4431.4255633, xyz[2])
        
if __name__ == '__main__':
    unittest.main()