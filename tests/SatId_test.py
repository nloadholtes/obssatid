#!/usr/bin/env python

'''
Created on Sept 1, 2010

@author: Nick Loadholtes
Copyright (c) 2010 Iron Bound Software. All rights reserved.
'''

import unittest
import SatId
from Observatory import Observatory
from sgp4_db_test import TLEReader

TLE_DATA = "1 25544U 98067A   02256.70033192  .00045618  00000-0  57184-3 0  1499\n\
2 25544  51.6396 328.6851 0018421 253.2171 244.7656 15.59086742217834\n\
\n\
# Above should give RA = 350.1615 deg, dec = -24.0241, dist = 1867.97542 km\n\
\n\
# Now compute a second,  higher satellite for the same place/time:\n\
1 19448U 88076D   02255.52918163 -.00000002  00000-0  10000-3 0  4873\n\
2 19448  65.7943 338.1906 7142558 193.4853 125.7046  2.04085818104610"

##  Set up desired lat/lon/ht in meters/JD,  specify a TLE,  and
##  the topocentric RA/dec/distance will be shown.
#lat 44.01
#lon -69.9
## Western longitudes are negative,  eastern positive
#ht 100
#JD 2452541.5         /* 24 Sep 2002 0h UT */

class TestSatId(unittest.TestCase):
    def setup(self):
        self.s = SatId.satId
        
    def testSatIdTest(self):
        '''This test is based on the obs_sat test program in the sat_obs project\
        from projectpluto.com'''
        
        satellites = TLE_DATA.splitlines()
        obs = Observatory()
        obs.initLatLong(44.01, -69.9)
        (ra,dec, dist) = SatId.satId(obs, satellites, 0)
        #RA = 350.1615 deg, dec = -24.0241, dist = 1867.97542 km
        self.assertEqual(350.1615, ra)
        self.assertEqual(-24.0241, dec)
        self.assertEqual(1867.97542, dist)
        
    #def testNotOverhead(self):
    #    self.fail("Not implemented")
    #
    #def testOverhead(self):
    #    self.fail("Not implemented")
