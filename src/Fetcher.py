'''
Created on Jul 19, 2010

@author: Nick Loadholtes
Copyright (c) 2010 Iron Bound Software. All rights reserved.
'''

import urllib2

class Fetcher(object):
    '''
    A class to get the TLE data from where ever it might be on the internets.
    '''


    def __init__(self, location):
        '''
        Constructor
        '''
        self.url = location
        
        
    def fetch(self):
        '''Go forth an gather thy data!'''
        u = urllib2.urlopen(self.url)
        data = u.read()
        u.close()
        print "Grabbed:",len(data)