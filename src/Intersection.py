'''
Created on Jul 19, 2010

@author: Nick Loadholtes
Copyright (c) 2010 Iron Bound Software. All rights reserved.
'''

from math import pi as PI, atan2, sqrt, asin

class Intersection(object):
    '''
    Computes if there is an intersection between a satellite and an observation
    from a specified location at a specified time.
    '''
    
    def isVisible(self, obs, satpos, radius):
        """A wrapper to simplify the checking of a satellite to see if it is visible from a given observtory."""
        ra, dec, dist = self.satelliteRaDec(obs, satpos)
        if dist < radius:
            return True
        return False
    
    def satelliteRaDec(self, obsloc, satpos):
        """Determines the RA and DEC of an object based on 
the location of the observer."""
        vec = []
        dist = 0.0
        ra = 0.0
        dec = 0.0
        
        #print("Satpos" + str(satpos) + " x "+ str(satpos.x))
        #print("Obsloc" + str(obsloc) + " x " + str(obsloc.x))
        x = satpos.x - obsloc.x
        vec.append(x)
        y = satpos.y - obsloc.y
        vec.append(y)
        z = satpos.z - obsloc.z
        vec.append(z)

        dist = sqrt((x*x) + (y*y) + (z*z))
        ra = atan2(y,x)

        if ra < 0.0:
            ra = PI + PI
        
        return (ra, dec, dist)