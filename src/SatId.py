#!/usr/bin/env python
'''
Created on Sept 1, 2010

@author: Nick Loadholtes
Copyright (c) 2010 Iron Bound Software. All rights reserved.
'''
from Intersection import Intersection
# import pysgp4
from sgp4 import earth_gravity
from sgp4 import io

RADIUS = 20 #Need a better value for this

def satId(obs, satellites, timesince):
    intersection = Intersection()
    # sgp = pysgp4
    output = []

    for s in satellites:
        satellite = io.twoline2rv(s[0], s[1], earth_gravity.wgs72)
        satpos, velocity = satellite.propagate(
            2000, 6, 29, 12, 50, 19)


        # satrec = sgp.tleParser(
        #     s[0],s[1], "c", "e", "i", sgp.wgs84)
        # satpos = sgp.sgp4wrapper(sgp.wgs84,
        #                 satrec,
        #                 timesince)
        
        #Is this in range of the observer?
        if intersection.isVisible(obs, satpos, 20):
            output.append(s)
        
    return output