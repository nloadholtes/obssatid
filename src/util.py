#!/bin/usr/env python
# encoding: utf-8
"""
util.py

various utility functions

Created by Nick Loadholtes on 04/16/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

from math import pi, degrees, radians, modf, sin, cos

EARTH_RADIUS_KM = 6378.135

def d2r(degree):
    '''Converts Degrees to Radians'''
    return radians(degree)
    
def r2d(radian):
    '''Converts Radians to Degrees'''
    return degrees(radian)
    
def d2h(degree):
    '''Converts Degrees to hours/min/sec'''
    #15 deg = 1 hour
    part, hours = modf(degree)
    sec, mins = modf(part * 60)
    output = str(int(hours)) + " " + str(int(mins)) + "' " + str(sec * 60) + "\""
    return output
    
    
def h2d(hours):
    '''Converts hours/min/sec to degrees'''
    pass

def latLongToXYZ(lat, long):
    '''Based on equations found on:http://astro.uchicago.edu/cosmus/tech/latlong.html '''
    la = lat * (pi / 180) 
    lon = long * (pi/180)
    x = EARTH_RADIUS_KM * cos(la) * cos(lon)
    z = EARTH_RADIUS_KM * sin(la)
    y = EARTH_RADIUS_KM * cos(la) * sin(lon)
    return (x, y, z)
