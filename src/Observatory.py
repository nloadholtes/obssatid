#!/usr/bin/env python
# encoding: utf-8
"""
Observatory.py

Handles the MPC Observatory file/codes.

Created by Nick Loadholtes on 04/08/2010.
Copyright (c) 2010 Iron Bound Software. All rights reserved.
"""

from util import *

class Observatory:
    
    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0
        
    def initLatLong(self, lat, long):
        (x,y,z) = latLongToXYZ(lat, long)
        self.x = x
        self.y = y
        self.z =z
        
    def parseData(self, data):
        """Takes the raw string data, returning a list of 
        lists of observation code data"""
        lines = data.splitlines()
        print len(lines)
        output = []
        for line in lines:
            row = line.split()
            output.append((row[0],row[1],row[2],row[3]," ".join(row[4:])))
        
        return output
    
    def convertToLat(self, c, s):
        """MPC provides the lat in sin/cos, this converts. Assumes a perfect sphere,
        so the altitude information is going to be way off.
        """
        from math import atan
        _c = float(c)
        _s = float(s)
        phi = atan(_s/_c)
        return r2d(phi) 
