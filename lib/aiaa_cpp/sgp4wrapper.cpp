/**
 * sgp4wrapper.cpp
 *
 * A wrapper around the AIAA SGP4 code. Simplifies access
 * to the code for SWIG and Python.
 *
 *Created by Nick Loadholtes on 06/12/2010.
 *Created Copyright (c) 2010 Iron Bound Software. All rights reserved.
 */

#include "sgp4wrapper.h"
#include "sgp4unit.h"
#include "sgp4io.h"

/**
* A wrapper for the SGP4 function call.
*/
satPos sgp4wrapper(
		gravconsttype whichconst, elsetrec& satrec,  double tsince)
{
	satPos *output = new(satPos);
	double r[3];
	double v[3];

	bool sgp4val = sgp4(whichconst, satrec, tsince,r, v);
	output->x = r[0];
	output->y = r[1];
	output->z = r[2];

	output->x_dot = v[0];
	output->y_dot = v[1];
	output->z_dot = v[2];

	return *output;
}

/**
* A wrapper for the twoline2rv satellite position data
*/
elsetrec tleParser(char longstr1[130], char longstr2[130],
      char typerun,  char typeinput, char opsmode,
      gravconsttype whichconst) 
{
	elsetrec *satrec = new(elsetrec);
	double startmfe = 0;
	double stopmfe = 0;
	double deltamin = 0;
	
	twoline2rv(longstr1, longstr2, typerun,  
		typeinput, opsmode, whichconst,
		startmfe, stopmfe, deltamin,
		*satrec);
	return *satrec;
}