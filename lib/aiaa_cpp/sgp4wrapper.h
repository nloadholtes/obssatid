/**
 * sgp4wrapper.h
 *
 * A wrapper around the AIAA SGP4 code to facilitate easier
 * interfacing via SWIG.
 *
 */

#include "sgp4unit.h"

typedef struct satPos {
	double x;
	double y;
	double z;

	double x_dot;
	double y_dot;
	double z_dot;

} satPos;

satPos sgp4wrapper(
		gravconsttype whichconst, elsetrec& satrec,  double tsince);

elsetrec tleParser(char longstr1[130], char longstr2[130],
      char typerun,  char typeinput, char opsmode,
      gravconsttype whichconst) ;