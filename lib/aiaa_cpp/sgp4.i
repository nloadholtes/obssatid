%module pysgp4
%{
/* Includes the header in the wrapper code */
#include "sgp4unit.h"
#include "sgp4io.h"
#include "sgp4wrapper.h"
%}

/* Parse the header file to generate wrappers */
%include "sgp4unit.h"
%include "sgp4io.h"
%include "sgp4wrapper.h"
